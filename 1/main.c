#include <stdio.h>

int main()
{
    int x = 0;
    int y = 0;
    while (x < 1000)
    {
        if ((x % 3 == 0) || (x % 5 == 0))
        {
            y = y + x;
        }
        x++;
    }
    printf("%d\n", y);
    return 0;
}
