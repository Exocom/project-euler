#include <stdio.h>

unsigned int fib(unsigned int a, unsigned int b)
{
    b = b + a;
    return b;
}

int main()
{
    unsigned int x = 1; // First Number
    unsigned int y = 2; // Second Number
    unsigned int z = 2; // Sum Total

    unsigned int t = 0; // Transitional 

    while (y <= 4000000)
    {
        t = y;
        y = fib(x, y);
        if (y % 2 == 0)
        {
            z = z + y;
        }
        x = t;
    }
    printf("%d\n", z);

    return 0;
}
